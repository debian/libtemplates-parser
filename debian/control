Source: libtemplates-parser
Section: libdevel
Priority: optional
Maintainer: Ludovic Brenta <lbrenta@debian.org>
Uploaders: Xavier Grave <xavier.grave@csnsm.in2p3.fr>,
 Nicolas Boulenguez <nicolas@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-ada-library (>= 9.1),
 dh-sequence-ada-library,
 gnat,
 gnat-14,
 gprbuild (>= 2025.0.0-2),
 libxmlada-dom-dev,
 libxmlada-input-dev,
 libxmlada-sax-dev,
 libxmlada-unicode-dev,
Build-Depends-Indep:
 dh-sequence-sphinxdoc,
 latexmk,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 tex-gyre,
 texinfo,
# Sphinx >= 1.6 uses the latexmk driver.
 texlive-fonts-recommended,
 texlive-latex-extra,
 texlive-latex-recommended,
# sphinx-doc requires iftex.sty:
 texlive-plain-generic,
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: https://github.com/AdaCore/templates-parser
Vcs-Browser: https://salsa.debian.org/debian/libtemplates-parser
Vcs-Git: https://salsa.debian.org/debian/libtemplates-parser.git

Package: libtemplates-parser-dev
Breaks: libtemplates-parser12-dev, libtemplates-parser13-dev, libtemplates-parser14-dev,
 libtemplates-parser15-dev, libtemplates-parser16-dev
Replaces: libtemplates-parser12-dev, libtemplates-parser13-dev, libtemplates-parser14-dev,
 libtemplates-parser15-dev, libtemplates-parser16-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${ada:Depends}, ${shlibs:Depends}, ${misc:Depends}
# No need to Depend: libxmlada*-dev
# because the generated project does not need to import "xmlada.gpr"
# because XMLada is only used by a body in templates-parser.
Suggests: libtemplates-parser-doc
Description: Ada library to parse files and replace variables
 The main goal is to ease the development of Web servers. In CGI
 (Common Gateway Interface) applications, you have to write  HTML pages in
 the program (in Ada or whatever other language) by using some specific
 libraries or by using only basic output functions.  This is of course not
 mandatory but by lack of a good library every Web development ends up doing
 just that.  Templates Parser takes that burden off of you.
 .
 This package contains the static libraries, documentation, tools and Ada
 specification files.

# On next so change, add a dash between the name and the SO
# version in order to silent a lintian warning.
Package: libtemplates-parser21
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada library to parse files and replace variables (runtime)
 The main goal is to ease the development of Web servers. In CGI
 (Common Gateway Interface) applications, you have to write  HTML pages in
 the program (in Ada or whatever other language) by using some specific
 libraries or by using only basic output functions.  This is of course not
 mandatory but by lack of a good library every Web development ends up doing
 just that.  Templates Parser takes that burden off of you.
 .
 This package contains the runtime shared library.

Package: libtemplates-parser-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Built-Using: ${sphinxdoc:Built-Using}
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Suggests: gnat
Description: Ada library to parse files and replace variables (documentation)
 The main goal is to ease the development of Web servers. In CGI
 (Common Gateway Interface) applications, you have to write  HTML pages in
 the program (in Ada or whatever other language) by using some specific
 libraries or by using only basic output functions.  This is of course not
 mandatory but by lack of a good library every Web development ends up doing
 just that.  Templates Parser takes that burden off of you.
 .
 This package contains the documentation for developers using the library.
